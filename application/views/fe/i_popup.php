<script type="text/javascript">
	var city = [];
	var citySelected;
	var kurir = [];
	var map_kec_kurir = [];

	var real_prc = real_prc_dsc = 0;

	$(function(){
		$.getJSON( "<?=base_url()?>djson/delivery", function( data ) {
			var items = "";
			$.each( data['mst_province'], function( key, val ) {
				items += '<option value='+key+'>'+val+'</option>'
			});
			$("#order-province").append(items);

			city = data['mst_city'];
			kurir = data['mst_kurir'];
		});

		$( "#order-province" ).change(function () {
			citySelected = city[$("#order-province").val()];
			var items="<option value='0'>Pilih Kota / Kab</option>";
			if (citySelected!=0) {
				$.each( citySelected, function( key, val ) {
					items += '<option value='+key+'>'+val+'</option>'
				});
			}
			$("#order-city").html(items);
			$("#order-courier").html("<option value='0'>Pilih Jasa Pengiriman</option>");
			$("#order-kecamatan").html("<option value='0'>Pilih Kecamatan</option>");
		});

		

		$( "#order-city" ).change(function () {
			cityID = $(this).val();
			kurirSelected = kurir[cityID];

			var csrf 				= $("#global-form").find("input");
			var usercode 			= $(this).parent().find('input');
			var opt 				= {};
			opt[csrf.attr('name')] 	= csrf.val();
			opt['city_id']			= cityID;
			$('body').css('cursor', 'wait');

			if (typeof map_kec_kurir[cityID] == 'undefined') {
				$.post("<?=base_url()?>home/get_kec",  opt,function(data){
					$('body').css('cursor', 'default');
					var li = [];
					var id_city = 0;
					$.each(data, function( key, val ) {
						li.push(val);
						id_city = val.id_city;
					});
					map_kec_kurir[id_city] = li;

					generate_kec();
				},"json");
			}else{
				generate_kec();
			}

			$("#order-courier").html("<option value='0'>Pilih Jasa Pengiriman</option>");
		})

		$( "#order-kecamatan" ).change(function () {
			var idx = $(this).find('option:selected').data('index_kec');
			var kurs = map_kec_kurir[cityID][idx];
			$("#hd-cust_kecamatan_id").val($(this).find('option:selected').data('id-kecamatan'));
			var items="<option value='0'>Pilih Jasa Pengiriman</option>";
			$.each(kurs.kurir, function( key, val ) {
				items += '<option data-kurir="'+val.name+'" data-kurir-price="'+val.price+'" value="'+val.name+'">'+val.name.toUpperCase()+' : Rp. '+formatNumber(val.price)+'</option>'
			});
			$("#order-courier").html(items);
		})

		function generate_kec(){
			var items="<option value='0'>Pilih Kecamatan</option>";
			$.each(map_kec_kurir[cityID], function( key, val ) {
				items += '<option data-index_kec="'+key+'" value="'+val.nama_kecamatan+'" data-id-kecamatan="'+val.id_kecamatan+'">'+val.nama_kecamatan+'</option>';
			});
			$("#order-kecamatan").html(items);
		}
		
		$('body').on('click','.buy-button',function (e) {
			e.preventDefault();
			//reset state
			$("#content-payment").hide();
			$("#content-confirmation").hide();
			$("#order-form").show();
			$("#order-form").trigger('reset');
			$(".order-field-error").removeClass('order-field-error');
			$(".select_payment.active").removeClass("active");
			$("#pay-sum-bank").hide();
			$("#pay-sum").hide();
			$("#pay-coupon-form").hide();
			$("#pay-coupon-valid").hide();
			$("#pay-coupon-have").show();
			$("#coupon-input").val('');
			payValue = "";

			initHiddenForm($(this));

			$("#pop-image").attr('src',$(this).data('image'));
			$("#pop-title").html($(this).data('title'));
			$("#pop-description").html($(this).data('description'));
			$("#pop-price").html(setPrice($(this)));
			$("#pop-description2").html($(this).data('description2'));
		});
	})
	
	function initHiddenForm(e){
		$("#hd-prd_id").val(e.data('id'));		
		$("#hd-prd_coupon_code").val('');
		$("#hd-prd_total").val('');
		$("#hd-payment").val('');
	}

	function setOrder(){
		$("#order-form").fadeOut('fast', function(){
			$(".select_payment").removeClass('order-field-error');
			$("#content-payment").fadeIn();
		});
	}

	function setPrice(ths){
		real_prc = ths.data('price');
		real_prc_dsc = ths.data('price_dsc');
		
		var prc = formatNumber(real_prc);
		var dsc = formatNumber(real_prc_dsc);
		
		if (real_prc_dsc > 0 ) {
			return '<span class="sale">Rp '+prc+'</span><span class="sale-price">Rp '+dsc+'</span>';
		}else{
			return '<span>Rp.'+prc+'</span>';
		}
	}
</script>

<style type="text/css">
#pop-price .sale:before {
	content: '';
	position: absolute;
	width: 100%;
	height: 2px;
	top: 18px;
	left: 0;
	background-color: #f9c80e;
}
#pop-price .sale-price {
	color: #f9c80e;
	margin-left: 10px;
}
#pay-coupon-valid .sale-price {
	color: #f9c80e;
	margin-left: 10px;
}
</style>

<section id="order" class="order mfp-hide">

	<div class="order-form-bg"></div>

	<!-- Start Form Header -->
	<div class="order-form-header clearfix">
		<div class="order-img">
			<img id="pop-image" src="" alt="product">
		</div>
		<div class="order-desc">
			<h2 id="pop-title"></h2>
			<p id="pop-description" class="version">Basic Green Version</p>
			<div id="pop-price"></div>
			<p id="pop-description2">Corporis suscipit dolorem, nisi, totam rem aperiam eaque ipsa.</p>
		</div>
	</div>
	<!-- End Form Header -->

	<div class="order-form-content">

		<!-- Start Order Form -->
		<?=form_open('/',array("class"=>"order-form","id"=>"order-form"))?>
			<input id="order-l-name" class="order-l-name" type="text" name="cust_l_name" placeholder="Last Name" style="display: none;" value="-">
			<input type="hidden" id="hd-prd_id" name="prd_id">
			<input type="hidden" id="hd-prd_coupon_code" name="prd_coupon_code">
			<input type="hidden" id="hd-prd_total" name="prd_total">
			<input type="hidden" id="hd-payment" name="payment" value="">
			<input type="hidden" id="hd-payment_detail" name="payment_detail" value="">
			<input type="hidden" id="hd-cust_kecamatan_id" name="cust_kecamatan_id" value="">

			<div class="col-md-12"><h3>Alamat Pengiriman</h3></div>
			<div class="col-md-12">
				<input id="order-f-name" class="order-f-name" type="text" name="cust_f_name" placeholder="Nama Lengkap">		
			</div>
			<div class="col-md-6">
				<input id="order-email" class="order-email" type="text" name="cust_email" placeholder="Email">
			</div>
			<div class="col-md-6">
				<input id="order-phone" class="order-phone" type="text" name="cust_phone" placeholder="Nomor Telepon">
			</div>
			<div class="col-md-12">
				<input id="order-address" class="order-address" type="text" name="cust_address" placeholder="Alamat Lengkap">
			</div>
			
			<div class="col-md-6">
				<select id="order-province" class="order-country" name="cust_province">
					<option value="0">Pilih Provinsi</option>
				</select>
			</div>

			<div class="col-md-6">
				<select id="order-city" class="order-country" name="cust_city">
					<option value="0">Pilih Kota / Kab</option>
				</select>
			</div>
			
			<div class="col-md-6">
				<select id="order-kecamatan" class="order-country" name="cust_kecamatan">
					<option value="0">Pilih Kecamatan</option>
				</select>
			</div>
			<div class="col-md-6">
				<input id="order-zip" class="order-zip" type="text" name="cust_zip" placeholder="Kode Pos">
			</div>
			<div class="col-md-6">
				<h3>Jasa Pengiriman</h3>
			</div>
			<div class="col-md-6">
				<h3>Jumlah Barang</h3>
			</div>
			<div class="col-md-6">
				<select id="order-courier" class="order-country" name="cust_shipping">
					<option value="0">Pilih Jasa Pengiriman</option>
				</select>
			</div>
			<div class="col-md-6">
				<input style="width: 80px;margin-top: 11px;" id="order-quantity" class="order-f-name" type="number" name="order_quantity" placeholder="qty" value="1" min="1"/>
				pcs
			</div>
			<div class="col-md-12 order-button-block">
				<button id="order-button" type="submit" class="button order-button" title="Order">Pesan Sekarang</button>
			</div>
		</form>

		<?=$this->load->view('fe/i_popup_payment');?>
		<?=$this->load->view('fe/i_confirm');?>
	</div>
</section>