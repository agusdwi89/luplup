<!-- Start Navigation -->
<nav id="navigation" class="navbar navbar-default navbar-top">
	<div class="container-fluid">

		<div class="nav-bg top-nav"></div>

		<div class="container">

			<? $topbutton = $this->db->get_where('site_config',array('name'=>'buy-now','subdomain'=>subdomain()))->row();?>

			<?php if ($topbutton->value2 == "on"): ?>
				<div class="navbar-button">
					<ul class="ul-btn">
						<li><a href="#<?=$topbutton->value;?>" class="button action-button-nav smooth" title="<?=$topbutton->value3;?>"><?=$topbutton->value3;?></a></li>
					</ul>
				</div>
			<?php endif ?>


			<div class="navbar-header">
				<button id="nav-icon" type="button" class="navbar-toggle collapsed nav-icon" data-toggle="collapse" data-target="#menu-navbar" aria-expanded="false">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</button>
				<a class="navbar-brand smooth" href="#home">
					<?=$this->db->get_where('site_config',array('id'=>2))->row()->value;?>
					<span class="logo-promo-text"><?=$this->db->get_where('site_config',array('id'=>3))->row()->value;?></span>
				</a> <!-- Title / Tagline -->
			</div>

			<div class="collapse navbar-collapse" id="menu-navbar">
				<ul class="nav navbar-nav navbar-right smooth-nav">
					<?
						$dbtpm = $this->db->get_where('top_menu',array('subdomain'=>subdomain()));
					?>
					<?php $i=0; foreach ($dbtpm->result() as $v): $i++;?>
						<li class="<?=($i==1) ? 'current' : '';?>"><a href="#<?=$v->url;?>" title="Home"><?=$v->name;?></a></li>	
					<?php endforeach ?>
				</ul>
			</div>


			<div class="clearfix"></div>

		</div>
	</div>
</nav>
<!-- End Navigation -->