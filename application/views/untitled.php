<div style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;width:100%!important;font-size:14px;color:#404040;margin:0;padding:0;max-width:600px;display:block;border-collapse:collapse;margin:0 auto;border:1px solid #e7e7e7">
	<table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:20px" bgcolor="transparent">
		<tbody>
			<tr style="margin:0;padding:0">
				<td>
					<h2>SHOPBAY.ID</h2>
				</td>
			</tr>
			<tr>
				<td style="text-align:center">
					<br>
				</td>
			</tr>
			<tr style="margin:0;padding:0">
				<td style="margin:0;padding:0" colspan="2">
					<h5 style="line-height:32px;color:#666;font-weight:900;font-size:24px;margin:0 0 16px;padding:0">
						Terimakasih telah berbelanja di shopbay.id
					</h5>
					<p style="font-weight:500;color:#666;font-size:14px;line-height:1.6;margin:0;padding:0">
						Berikut ringkasan pesanan anda :
						<span style="color:#ff5722">
						</span>
					</p>
				</td>
			</tr>
			<tr style="margin:0;padding:0">
				<td style="margin:0;padding:0" colspan="2">
				</td>
			</tr>
		</tbody>
	</table>
	<table style="width:100%;margin-bottom:24px;padding:0 20px">
		<thead>
			<tr>
				<td style="width:50%">
					<span style="font-size:12px;color:#999;font-weight:normal;word-break:break-word">Total Pembayaran</span>
				</td>
				<td style="width:50%">
					<span style="font-size:12px;color:#999;font-weight:normal;word-break:break-word">Waktu Pemesanan</span>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="vertical-align:top">
					<span style="font-size:24px;color:#666;font-weight:600;word-break:break-word">Rp <?=format_number($db->prd_total);?></span>
				</td>
				<td style="vertical-align:top">
					<span style="font-size:16px;color:#666;word-break:break-word"><?=pretty_date($db->time_order);?></span>
				</td>
			</tr>
		</tbody>
	</table>
	<table style="width:100%;margin-bottom:24px;padding:0 20px">
		<thead>
			<tr>
				<td style="width:50%">
					<span style="font-size:12px;color:#999;font-weight:normal;word-break:break-word">ORDER ID</span>
				</td>
				<td style="width:50%">
					<span style="font-size:12px;color:#999;font-weight:normal;word-break:break-word">Metode Pembayaran</span>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="vertical-align:top">
					<span style="font-size:24px;color:#666;font-weight:600;word-break:break-word">SB1<?=sprintf("%'.05d\n", $db->id)?></span>
				</td>
				<td style="vertical-align:top">
					<span style="font-size:16px;color:#666;word-break:break-word">
							<?php if ($db->payment == "cod"): ?>
								Cash on Delivery
							<?php elseif($db->payment == "transfer"): ?>
								Bank Transfer
							<?php endif ?>
					</span>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr>
				<td style="vertical-align:top" colspan="2">

					<span style="font-size:16px;color:#666;word-break:break-word">
						<?php if ($db->payment == "cod"): ?>
							<b>Pembayaran dilakukan ketika barang sampai dirumah</b>
						<?php endif ?>
					</span>

				</td>
				<td style="vertical-align:top">
				</td>
			</tr>
		</tbody>
	</table>

	<table style="margin:24px 0;padding:0 20px;width:100%">
		<tbody>
			<tr>
				<td style="width:100%;padding-right:8px;vertical-align:top">
					<?php if (isset($email_state)): ?>
						<?php if ($email_state == "payment-success"): ?>
							<br>
							<h2 style="text-align: center;color:#42b549">Selamat Pembayaran Diterima</h2>
							<br>
						<?php elseif($email_state == "delivery-process"): ?>
						<?php else: ?>
						<?php endif ?>	
					<?php endif ?>
					
					<a href="<?=base_url()?>home/order/<?=$db->link_unique;?>" style="padding:16px 0;background:#42b549;color:#fff;border:1px solid #42b549;text-decoration:none;display:inline-block;border-radius:4px;width:100%;text-align:center" target="_blank" data-saferedirecturl="<?=base_url()?>home/order/<?=$db->link_unique;?>">Cek Status Pemesanan</a>
				</td>
			</tr>
		</tbody>
	</table>

	<div style="padding:0 0 0 20px">
		<div style="border-top:1px solid #e0e0e0;padding:8px 0">
			<h2 style="font-size:14px;font-weight:normal;color:#666">Detail Pemesanan</h2>
			<span style="width:24px;border:2px solid #ff5722;display:inline-block;margin-left:0"></span>
		</div>

		<table style="width:100%;max-width:600px;padding-bottom:18px;padding:10px 20px 10px 0;border-bottom:1px solid #e0e0e0">
			<tbody>
				<tr style="margin:0">
					<td style="vertical-align:top;text-align: center;" colspan="2">
						<img width=150 src="<?=base_url()?>assets/section/<?=$db->prd_image;?>">
						<br><br>
					</td>
				</tr>
				<tr style="margin:0">
					<td style="vertical-align:top">
						<p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;font-weight:normal">Produk : <b><?=$db->prd_title;?></b></p>
					</td>
					<td style="vertical-align:top;text-align:right;width:120px">
						<span style="float:left;font-size:14px;color:#999;margin:0;font-weight:normal">Rp</span>
						<p style="float:right;font-size:14px;color:#999;margin:0;font-weight:normal"><?=format_number($db->prd_price);?></p>
					</td>
				</tr>
				<?php if ($db->prd_coupon > 0): ?>
					<tr>
						<td style="vertical-align:top">
							<p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px">Penggunaan Kupon : <b><?=$db->prd_coupon_code;?></b></p>
						</td>
						<td style="vertical-align:top;text-align:right;width:120px">
							<span style="float:left;font-size:14px;color:#999;margin-top:5px">- Rp</span>
							<p style="float:right;font-size:14px;color:#999;margin-top:5px"><?=format_number($db->prd_coupon)?></p>
						</td>
					</tr>
				<?php endif ?>
			</tbody>
		</table>

		<table style="width:100%;max-width:600px;padding:0 20px 0 0;border-bottom:1px solid #e0e0e0;margin:0">
			<tbody>
				<tr>
					<td style="padding:8px 0 0;vertical-align:top">

					</td>
				</tr>
				<tr>
					<td style="vertical-align:top">
						<p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px">Pengiriman : <b><?=$db->cust_shipping;?></b></p>
					</td>
					<td style="vertical-align:top;text-align:right;width:120px">
						<span style="float:left;font-size:14px;color:#999;margin-top:5px">Rp</span>
						<p style="float:right;font-size:14px;color:#999;margin-top:5px"><?=format_number($db->prd_shipment);?></p>
					</td>
				</tr>
			</tbody>
		</table>


		<table style="width:100%;max-width:600px;padding-bottom:18px;padding:10px 20px 10px 0">
			<tbody>
				<tr>
					<td style="vertical-align:top">
						<p style="font-size:14px;color:#666;font-weight:bold;padding:0;margin:0">Tagihan</p>
					</td>
					<td style="vertical-align:top;text-align:right;width:120px">
						<span style="float:left;font-size:14px;color:#666;margin:0;font-weight:bold">Rp</span>
						<p style="float:right;font-size:14px;color:#666;margin:0;font-weight:bold"><?=format_number($db->prd_total);?></p>
					</td>
				</tr>
			</tbody>
		</table>

	</div>

	<div style="padding:0 20px;margin-bottom:16px;margin-top:8px">
		<div style="font-size:14px;line-height:20px;text-align:center;color:#999;padding:16px 24px;border:1px solid rgb(255,227,143);border-radius:4px;background:#fff7d7">

			<?php if ($db->payment == "cod"): ?>
				<p><?=$this->db->get_where('mst_payment',array('id'=>1))->row()->description;?></p>
			<?php elseif($db->payment == "transfer"): ?>
				<p><?=$this->db->get_where('mst_payment',array('id'=>2))->row()->description;?></p>
				<b style="margin-bottom: 8px;display: block;"><?=$db->payment_detail;?></b>
			<?php endif ?>

		</div>
	</div>

	<div style="padding:0 20px">
		<p style="font-size:14px;color:#999;padding:16px 0;margin:0;border-top:1px solid #e0e0e0">
			Email dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
		</p>
	</div>


	<table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;clear:both!important;background-color:transparent;margin:0 0 10px;padding:0" bgcolor="transparent">
		<tbody>

			<tr style="margin:0;padding:0">
				<td style="margin:0;padding:0"></td>
				<td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0px">
				</td>
			</tr>
		</tbody>
	</table>
	<table align="center">
		<tbody><tr style="margin:0;padding:0 0 0 0">
			<td style="display:block!important;width:600px!important;clear:both!important;margin:0 auto;padding:0">
				<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;background-color:#f7f7f7;font-size:13px;color:#999999;border-top:1px solid #dddddd">
					<tbody><tr>
						<td width="600" align="center" style="padding:30px 20px 0"><?=$this->db->get_where('site_config',array('id'=>8))->row()->value;?></td>
					</tr>
					<tr>
						<td width="600" align="center" style="padding:10px 20px 30px">
							&copy; 2019, <span class="il">shopbay.id</span>
						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
	</tbody>
</table>
</div>