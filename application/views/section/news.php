<style type="text/css">
	#flexreview{
		background: transparent !important;
	}
	#flexreview .flex-next,#flexreview .flex-prev{
		font-family: 'simple-line-icons';
		speak: none;
		font-style: normal;
		font-weight: normal;
		font-variant: normal;
		text-transform: none;
		line-height: 1;
		/* Better Font Rendering =========== */
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
	}
	div.review{padding-left: 50px;}
	#flexreview .flex-prev:before{content: "\e605" !important;font-family: 'simple-line-icons' !important; color:#4cc3d3;}
	#flexreview .flex-next:before{content: "\e606" !important;font-family: 'simple-line-icons' !important; color:#4cc3d3;}	
</style>

<? 
	$news = $this->db->limit(5)->get_where('news',array('subdomain'=>subdomain()));
?>

<section id="news-thumb" class="section border-0 m-0 pb-3 reviews block">
	<div class="container container-lg">
		<div class="row pb-1">

			<h2>News</h2>
			<div class="block-desc">Dapatkan berita / kabar terbaru dari kami</div>

			<?php foreach ($news->result() as $n): ?>
				<div class="col-sm-6 col-lg-4 mb-4 pb-2">
					<a href="<?=base_url()?>news/<?=$n->slug;?>">
						<article>
							<div class="thumb-info thumb-info-no-borders thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom border-radius-0">
								<div class="thumb-info-wrapper thumb-info-wrapper-opacity-6">
									<img src="<?=base_url()?>assets/news/<?=$n->img_thumbnail;?>" class="img-fluid" alt="<?=$n->title;?>">
									<div class="dates"><?=pretty_date($n->date,false);?></div>
									<div class="thumb-info-title bg-transparent p-4">
										<div class="thumb-info-inner mt-1">
											<h2 class="text-color-light line-height-2 text-4 font-weight-bold mb-0"><?=$n->title;?></h2>
										</div>
										<div class="thumb-info-show-more-content">
											<p class="mb-0 text-1 line-height-9 mb-1 mt-2 text-light opacity-5"><?=smart_trim($n->content,150);?></p>
										</div>
									</div>
								</div>
							</div>
						</article>
					</a>
				</div>	
			<?php endforeach ?>

			<div id="more-news" class="col-sm-6 col-lg-4 mb-4 pb-2">
				<a href="<?=base_url('news')?>">
					<article>
						<div class="thumb-info thumb-info-no-borders thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom border-radius-0">
							<div class="thumb-info-wrapper thumb-info-wrapper-opacity-6">
								<span>Berita<br>Lain . . .</span>
							</div>
						</div>
					</article>
				</a>
			</div>

		</div>
	</div>
</section>