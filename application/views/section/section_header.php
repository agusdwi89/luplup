<header id="header" class="header-container">
	<div class="container-fluid header-content">
		<span class="trapeze-bg"></span>
		<div class="container screen-container">
			<div class="row">
				<div class="col-md-12 title-center-content">
					<div class="col-md-6 title-text">
						<h1>
							<?=$data->title_first;?> <span class="text-notice-line"><?=$data->title_highlight;?></span> <?=$data->title_last;?>
						</h1>
						<p>
							<?=$data->description;?>
						</p>

						<?php if ($data->button_type == "section"): ?>
							<a id="btn_section_header_<?=$data->id;?>" href="#<?=$data->button_link;?>" class="button hero-button smooth" title="<?=$data->button_text;?>"><?=$data->button_text;?></a>
						<?php else: ?>
							<a id="btn_section_header_<?=$data->id;?>" href="<?=$data->button_link;?>" class="button hero-button" title="<?=$data->button_text;?>" target="_blank"><?=$data->button_text;?></a>
						<?php endif;?>
					</div>
					<div class="col-md-6 title-product">
						<span>Product</span>
						<img src="<?=base_url()?>assets/section/<?=$data->image?>" alt="product">
					</div>
				</div>
				<nav class="scroll-link">
					<a href="#about" class="smooth" title="Scroll">Scroll</a>
				</nav>
			</div>
		</div>
	</div>
</header>	

<?$this->button_script_id['section_header'][] = $data->id;?>