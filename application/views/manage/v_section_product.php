<script type="text/javascript">
    $(function(){
        $('[data-plugin="switchery"]').each(function (idx, obj) {
            new Switchery($(this)[0], $(this).data());
        });

        $('.checkcolor').change(function() {
            var dataf = {};
            var gbl = $("#global-form").find('input');
            document.body.style.cursor='wait';
            var fstatus = "no";
            var fid     = $(this).data('id');
            if($(this).is(":checked")) 
                fstatus = "yes";

            dataf['available']      = fstatus;
            dataf['id']             = fid;
            dataf[gbl.attr('name')] = gbl.val();

            $.post( "<?=base_url()?>manage/section_product/set_off", dataf).done(function( data ) {
                document.body.style.cursor='default';
            });

        });
    })
</script>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Product</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Product Section</h4>
                        <br>
                        <?=form_open('',array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="col-md-12 control-label">Title</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="title first" class="form-control" name="title" value="<?=$master->title;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-12 control-label">Description</label>
                                <div class="col-md-12">
                                    <textarea placeholder="Description text" class="form-control" rows="5" name="description"><?=$master->description;?></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Add Product Items</h4>
                        <br>
                        <?=form_open_multipart('manage/section_product/add_item',array("class"=>"form-horizontal"))?>
                            <input type="hidden" name="prd_id" value="<?=$id;?>"> 
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Title</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="title text" class="form-control" name="title" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Product Code</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="code" class="form-control" name="code" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Description</label>
                                <div class="col-md-10">
                                    <textarea style="min-height:30px;height:30px" placeholder="Description text" class="form-control" rows="1" name="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Description Optional</label>
                                <div class="col-md-10">
                                    <textarea style="min-height:30px;height:30px" placeholder="Description text" class="form-control" rows="1" name="description2"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">image product</label>
                                <div class="col-md-10">
                                    <input type="file" class="default" name="userfile[]">
                                    <p class="text-muted m-b-25">* Image size up to 196 x 225 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">image background</label>
                                <div class="col-md-10">
                                    <input type="file" class="default" name="userfile[]">
                                    <p class="text-muted m-b-25">* Image size up to 768 x 500 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Price / Discount</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Price" class="form-control" name="price" value="">
                                </div>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Discounted Price" class="form-control" name="price_dsc" value="">
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-reload" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Clear</button>
                        <?=form_close()?>
                        <br>
                        <table class="table table-space m-0">   
                            <thead>
                                <tr>
                                    <th>Img Product</th>
                                    <th>Img Bg</th>
                                    <th>Title / Description</th>
                                    <th>Price / Disc</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?foreach ($items->result() as $k):?>
                                    <tr>
                                        <td>
                                            <a href="<?=base_url()?>assets/section/<?=$k->image;?>" target="_blank">
                                                <img width=100 src="<?=base_url()?>assets/section/<?=$k->image;?>">
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?=base_url()?>assets/section/<?=$k->image_bg?>" target="_blank">
                                                <img width=100 src="<?=base_url()?>assets/section/<?=$k->image_bg;?>">
                                            </a> 
                                        </td>
                                        <td>
                                            <span style="font-size: 14px;background-color: grey;" class="badge badge-inverse"><?=$k->code;?></span><br>
                                            <b><?=$k->title;?></b>
                                            <p>- <?=$k->description;?></p>
                                            <p>- <?=$k->description2;?></p>
                                        </td>
                                        <td>
                                            <?if ($k->price_dsc > 0): ?>
                                                <span class="sale">
                                                    <strike>Rp.<?=$k->price;?></strike>
                                                </span>
                                                <span class="sale-price">Rp.<?=$k->price_dsc;?></span>
                                            <?else:?>
                                                <span>Rp.<?=$k->price;?></span>
                                            <?endif;?>
                                        </td>
                                        <td>
                                            <span class="dz-tip" title="Turn off to set sold out">
                                                <input data-id="<?=$k->id;?>" class="checkcolor" type="checkbox" data-plugin="switchery" data-color="#ff5d48"  <?=($k->available == "yes") ? 'checked' : '';;?>/>
                                            </span>
                                        </td>
                                        <td>
                                            <center>
                                                <a style="opacity:100 !important;margin-left: 5px" title="delete" href="<?=base_url()?>manage/section_product/delete_item/<?=$k->id;?>/<?=$id;?>" class="fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                                <a id="edit-mdl<?=$k->id;?>" data-toggle="modal" data-target=".bs-example-modal-lg" style="opacity:100 !important" title="edit" class="fa fa-pencil-square-o delete-list dz-tip edit-mdl"
                                                     href="<?=base_url()?>manage/section_product/edit_item/<?=$k->id;?>"
                                                     data-mdl-title="<?=$k->title;?>"
                                                     data-mdl-code="<?=$k->code;?>"
                                                     data-mdl-description="<?=$k->description;?>"
                                                     data-mdl-description2="<?=$k->description2;?>"
                                                     data-mdl-price="<?=$k->price;?>"
                                                     data-mdl-price_dsc="<?=$k->price_dsc;?>"
                                                ></a>
                                            </center>
                                        </td>
                                    </tr>
                                <?endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('.modal').on('show.bs.modal', function (e) {
            var id = e.relatedTarget.id;
            var ths = $("#"+id);
            var uri = ths.attr('href');
            $("#mdl-title").val(ths.data('mdl-title'));
            $("#mdl-code").val(ths.data('mdl-code'));
            $("#mdl-description").val(ths.data('mdl-description'));
            $("#mdl-description2").val(ths.data('mdl-description2'));
            $("#mdl-price").val(ths.data('mdl-price'));
            $("#mdl-price_dsc").val(ths.data('mdl-price_dsc'));
            $("#mdl-form").attr('action',uri);
        });
    })
</script>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title mt-0" id="myLargeModalLabel">Edit Product</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?=form_open_multipart('',array("class"=>"form-horizontal","id"=>"mdl-form"))?>
                    <input type="hidden" name="prd_id" value="<?=$id;?>"> 
                    <div class="form-group row">
                        <label class="col-md-2 control-label">Title</label>
                        <div class="col-md-5">
                            <input id="mdl-title" type="text" placeholder="title text" class="form-control" name="title" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 control-label">Product Code</label>
                        <div class="col-md-5">
                            <input id="mdl-code" type="text" placeholder="code" class="form-control" name="code" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 control-label">Description</label>
                        <div class="col-md-10">
                            <textarea id="mdl-description" style="min-height:30px;height:30px" placeholder="Description text" class="form-control" rows="1" name="description"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 control-label">Description Optional</label>
                        <div class="col-md-10">
                            <textarea id="mdl-description2" style="min-height:30px;height:30px" placeholder="Description text" class="form-control" rows="1" name="description2"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-2">image product</label>
                        <div class="col-md-10">
                            <img class="mdl-img" id="userfile1" src="">
                            <input type="file" class="default" name="userfile[]">
                            <p class="text-muted m-b-25">* Image size up to 196 x 225 PX , JPG & PNG allowed.</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-2">image background</label>
                        <div class="col-md-10">
                            <img class="mdl-img" id="userfile2" src="">
                            <input type="file" class="default" name="userfile[]">
                            <p class="text-muted m-b-25">* Image size up to 768 x 500 PX , JPG & PNG allowed.</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 control-label">Price / Discount</label>
                        <div class="col-md-5">
                            <input id="mdl-price" type="text" placeholder="Price" class="form-control" name="price" value="">
                        </div>
                        <div class="col-md-5">
                            <input id="mdl-price_dsc" type="text" placeholder="Discounted Price" class="form-control" name="price_dsc" value="">
                        </div>
                    </div>
                    <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                    <button id="btn-reload" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Clear</button>
                <?=form_close()?>
            </div>
        </div>
    </div>
</div>