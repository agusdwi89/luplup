<?=$this->load->view('include/froala_editor');?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage News</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit News</h4>
                        <br>
                        <?=form_open_multipart('',array("class"=>"form-horizontal"))?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label class="col-md-2 control-label">Title</label>
                                        <div class="col-md-10">
                                            <input type="text" placeholder="title" class="form-control" name="title" value="<?=$item->title;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 control-label">Meta Keyword</label>
                                        <div class="col-md-10">
                                            <input type="text" placeholder="keyword" class="form-control" name="meta_keyword" value="<?=$item->meta_keyword;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 control-label">Meta Description</label>
                                        <div class="col-md-10">
                                            <input type="text" placeholder="description" class="form-control" name="meta_description" value="<?=$item->meta_description;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Thumbnail</label>
                                        <div class="col-md-10">
                                            <?if ($item->img_thumbnail != ""): ?>
                                                <a target="_blank" href="<?=base_url()?>assets/news/<?=$item->img_thumbnail;?>">
                                                    <img class="image-section-header-placeholder" src="<?=base_url()?>assets/news/<?=$item->img_thumbnail;?>">
                                                </a>
                                                <span>change image : </span>
                                            <?endif;?>
                                            <input type="file" class="default" name="img_thumbnail">
                                            <p class="text-muted m-b-25">* Image size up to 100 x 100 PX , JPG & PNG allowed.</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Header</label>
                                        <div class="col-md-10">
                                            <?if ($item->img_header != ""): ?>
                                                <a target="_blank" href="<?=base_url()?>assets/news/<?=$item->img_header;?>">
                                                    <img class="image-section-header-placeholder" src="<?=base_url()?>assets/news/<?=$item->img_header;?>">
                                                </a>
                                                <span>change image : </span>
                                            <?endif;?>
                                            <input type="file" class="default" name="img_header">
                                            <p class="text-muted m-b-25">* Image size up to 900 x 400 PX , JPG & PNG allowed.</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 control-label">Content</label>
                                        <div class="col-md-10">
                                            <textarea id='edit2' class="selector" style="margin-top: 30px;" placeholder="Type some text" name="content"><?=$item->content;?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <a href="<?=base_url('manage/news')?>" class="btn btn-secondary waves-effect waves-light m-t-20">Back to News List</a>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>