<style type="text/css">
	#datatable_wrapper{opacity: 0}
	#datatable{opacity: 0}
</style>
<script type="text/javascript">
	$(function(){
		$('#datatable').dataTable({
			"initComplete": function(settings, json) {
				$("#loader").remove();
				$("#datatable_wrapper").css('opacity', '1');
				$("#datatable").css('opacity', '1');
			}
		});
	})
</script>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 class="page-title">Manage News</h4>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body table-responsive">
						<h4 class="m-t-0 header-title">
							<b>List of news</b>
							<a href="<?=base_url('manage/news/add')?>" style="float: right" class="btn btn-info waves-effect waves-light btn-sm"> <i class="fa fa-plus m-r-5"></i> <span>Add News</span> </a>
						</h4>
						<br>
						<div id='loader'><center>Loading data . . . <br></center></div>

						<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
							<thead>
								<tr>
									<th>No</th>
									<th>Thumbnail</th>
									<th>Title</th>
									<th>Content</th>
									<th>Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?$i=0;foreach ($db->result() as $v): $i++;?>
									<tr>
										<td><?=$i;?></td>
										<td>
											<center>
												<img width=75 src="<?=base_url()?>assets/news/<?=$v->img_thumbnail;?>">
											</center>
										</td>
										<td><?=$v->title;?></td>
										<td><?=smart_trim($v->content,50)?></td>
										<td><?=format_date_time($v->date)?></td>
										<td>
											<center>
												<a href="<?=base_url()?>manage/news/edit/<?=$v->id;?>" class="btn btn-sm btn-icon waves-effect waves-light btn-purple dz-tip" title="Edit News"> <i class="fa fa-pencil"></i> </a>
												<a href="<?=base_url()?>manage/news/delete/<?=$v->id;?>" class="btn btn-sm btn-icon waves-effect waves-light btn-danger dz-tip confirm-delete" title="Delete News"> <i class="fa fa-trash"></i> </a>
											</center>
										</td>
										
									</tr>
								<?endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>