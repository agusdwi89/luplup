<script type="text/javascript">
	$(function(){
		$('body').on('click','.list-group-item',function (e) {
			$('.list-group-item.active').removeClass('active');
			$(this).addClass('active');
			
			if($(this).data('code') == "custom"){
				$("#wizard-vertical").hide();
				$("#wizard-vertical2").show();
			}else{
				$("#wizard-vertical2").hide();
				var frm = $("#wizard-vertical")
				frm.show();
				frm.find('h4').text($(this).data('name'));
				$("#img-large-theme").attr('src',$(this).find('img').attr('src'));
			}
		});

		$("#<?=$theme_choosen;?>").trigger('click');

		$('body').on('click','#saveform',function (e) {
			e.preventDefault();
			$("#hd-theme").val($('.list-group-item.active').data('code'));
			$("#frm_theme").submit();
		});

		$('.colorpicker-default').colorpicker({format: 'hex'});
	})
</script>
<?=form_open('',array('id'=>'frm_theme'))?>
	<input type="hidden" id="hd-theme" name="theme">


<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 class="page-title">Manage Theme Color</h4>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body table-responsive">

						<?php if ($this->sub_domain == "all"): ?>

							<div class="col-md-12">
								<center>
									<br>
									<h3>please select sub domain above</h3>
									<br>
								</center>
							</div>

						<?php else: ?>

						<h4 class="m-t-0 header-title">
							<b>List of Available Theme</b>
							<a id="saveform" title="save setting" href="#" style="float: right;margin-right:5px" class="btn btn-info waves-effect waves-light btn-sm dz-tip">
								<i class="fa fa-pencil"></i>
								<span>save setting</span>
							</a>
						</h4>
						<p><i>*click each theme color to detail option</i></p>
						<div class="row">
							<div class="col-sm-5">
								<ul id="list-payment" class="list-group">
									<?for ($i=1;$i<7;$i++): ?>
										<li id="color-<?=$i;?>" class="list-group-item d-flex align-items-center" data-code="color-<?=$i;?>" data-name="Theme Color-<?=$i;?>">
											<img height="50" src="<?=base_url()?>assets/theme/color-<?=$i;?>.jpg">	
											<span>Theme Color-<?=$i;?></span>
										</li>
									<?endfor;?>
									<li id="custom" class="list-group-item d-flex align-items-center" data-code="custom" data-name="Custom Theme">
										<img height="50" src="<?=base_url()?>assets/theme/custom.jpg">	
										<span>Custom Theme</span>
									</li>
								</ul>
							</div>
							<div class="col-sm-7">
								<div class="">
									<section id="wizard-vertical" role="tabpanel" aria-labelledby="wizard-vertical-h-0" class="body current" aria-hidden="false">
										<?=form_open('',array('id'=>'desc_form'))?>
											<h4>Theme Color</h4>
											<div class="form-group">
												<br>
												<center>
													<img id="img-large-theme" height="150" src="">
												</center>
											</div>
										<?=form_close()?>
									</section>
									<section id="wizard-vertical2" role="tabpanel" aria-labelledby="wizard-vertical-h-0" class="body current" aria-hidden="true">
										<h4>Custom Color</h4>
										<?foreach ($custom_color as $key => $value): ?>
											<div class="form-group">
												<label><?=$key;?></label>
												<div data-color-format="hex" data-color="<?=$value;?>" class="colorpicker-default input-group">
													<input name="custom[<?=$key;?>]" type="text" value="" class="form-control">
													<span class="input-group-append add-on">
														<i style="background-color:<?=$value;?>;margin-top: 4px;"></i>
													</span>
												</div>
											</div>
										<?endforeach;?>
									</section>
								</div>							
							</div>
						</div>
						<?endif?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?=form_close()?>