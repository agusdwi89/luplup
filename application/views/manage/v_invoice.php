<style type="text/css">
    #resi-delivery input{
        width: 200px !important;
        height: auto;
        font-size: 10px;
        padding: 5px;
        display: inline;
    }
</style>
<script type="text/javascript">
    $(function(){
        $('body').on('click','.set_payment',function (e) {
            <?if ($db->payment == "code"):?>
                return confirm("Are you sure update this order to order confirmed ? \rYou can't undo this action\rCustomer immidately receive an email update status");
            <?else :?>
                return confirm("Are you sure update this order to payment success ? \rYou can't undo this action\rCustomer immidately receive an email update status");
            <?endif?>
        });
        $('body').on('click','.set_deliver',function (e) {
            return confirm("Are you sure update this order to delivery process ? \rYou can't undo this action\rCustomer immidately receive an email update status");
        });
        $('body').on('click','.set_completed',function (e) {
            return confirm("Are you sure update this order to completed ? \rYou can't undo this action\rCustomer immidately receive an email update status");
        });

        $('body').on('click','#btn-save-resi',function (e) {
            var url = $(this).attr('href');
            var csrf                = $("#global-form").find("input");
            var resi                = $("#text-resi").val();
            var opt                 = {};
            opt[csrf.attr('name')]  = csrf.val();
            opt['resi']             = resi;
            $.post(url,  opt,function(data){});
            return false;
        });
    })
</script>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Detail Order</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="clearfix">
                            <div class="float-left">
                                <img src="assets/images/logo_dark.png" alt="" height="30">
                            </div>
                            <h3 class="m-0">
                                Order :: #SB1<?=sprintf('%05d', $db->id);?> - <?=$db->cust_f_name;?>
                                <a id="btn-save-order" href="<?=base_url()?>manage/order" class="badge badge-info">order list</a>
                                <a id="btn-save-order" target="_blank" href="<?=base_url()?>manage/order/print/<?=$db->id;?>" class="badge badge-info">print</a>
                            </h3>
                        </div>

                        <div class="row m-t-30">
                            <div class="col-8">
                                <h5>Order Detail</h5>

                                <div >
                                    <p style="margin-bottom:5px"><small><strong>Order Date: </strong></small> <?=pretty_date($db->time_order);?></p>
                                    <p style="margin-bottom:5px"><small><strong>Order ID: </strong></small> #SB1<?=sprintf('%05d', $db->id);?></p>
                                    <div style="margin-bottom:5px">
                                        <small><strong>Order Status: </strong></small> 
                                        <div class="btn-group m-l-10">
                                            <?php if ($db->payment == "cod"): ?>

                                                <?if ($db->order_status == 'wait-payment'): ?>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">menunggu konfirmasi</span></a>
                                                    <a class="set_payment dz-tip" title="update status to order confirmed" href="<?=base_url()?>manage/order/set_payment/<?=$db->id;?>"><span style="padding: 5px 7px;" class="badge badge-inverse">konfirmasi pesanan</span></a>
                                                    <a class="set_deliver dz-tip" title="update status to delivery process" href="<?=base_url()?>manage/order/set_deliver/<?=$db->id;?>"><span style="padding: 5px 7px;" class="badge badge-inverse">proses pengiriman</span></a>
                                                    <a class="set_completed dz-tip" title="update status to completed"href="<?=base_url()?>manage/order/set_completed/<?=$db->id;?>"><span style="padding: 5px 7px;" class="badge badge-inverse">pembayaran diterima & pengiriman selesai</span></a>
                                                <?elseif($db->order_status == 'payment-success'):?>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">menunggu konfirmasi</span></a>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">konfirmasi pesanan</span></a>
                                                    <a class="set_deliver dz-tip" title="update status to delivery process" href="<?=base_url()?>manage/order/set_deliver/<?=$db->id;?>"><span style="padding: 5px 7px;" class="badge badge-inverse">proses pengiriman</span></a>
                                                    <a class="set_completed dz-tip" title="update status to completed"href="<?=base_url()?>manage/order/set_completed/<?=$db->id;?>"><span style="padding: 5px 7px;" class="badge badge-inverse">pembayaran diterima & pengiriman selesai</span></a>
                                                <?elseif($db->order_status == 'delivery-process'):?>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">menunggu konfirmasi</span></a>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">konfirmasi pesanan</span></a>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">proses pengiriman</span></a>
                                                    <a class="set_completed dz-tip" title="update status to completed"href="<?=base_url()?>manage/order/set_completed/<?=$db->id;?>"><span style="padding: 5px 7px;" class="badge badge-inverse">pembayaran diterima & pengiriman selesai</span></a>
                                                <?else: ?>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">menunggu konfirmasi</span></a>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">konfirmasi pesanan</span></a>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">proses pengiriman</span></a>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">pembayaran diterima & pengiriman selesai</span></a>
                                                <?endif;?>

                                            <?php else: ?>

                                                <?if ($db->order_status == 'wait-payment'): ?>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">menunggu pembayaran</span></a>
                                                    <a class="set_payment dz-tip" title="update status to payment success" href="<?=base_url()?>manage/order/set_payment/<?=$db->id;?>"><span style="padding: 5px 7px;" class="badge badge-inverse">pembayaran sukses</span></a>
                                                    <a class="set_deliver dz-tip" title="update status to delivery process" href="<?=base_url()?>manage/order/set_deliver/<?=$db->id;?>"><span style="padding: 5px 7px;" class="badge badge-inverse">proses pengiriman</span></a>
                                                    <a class="set_completed dz-tip" title="update status to completed"href="<?=base_url()?>manage/order/set_completed/<?=$db->id;?>"><span style="padding: 5px 7px;" class="badge badge-inverse">pessanan diterima</span></a>
                                                <?elseif($db->order_status == 'payment-success'):?>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">menunggu pembayaran</span></a>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">pembayaran sukses</span></a>
                                                    <a class="set_deliver dz-tip" title="update status to delivery process" href="<?=base_url()?>manage/order/set_deliver/<?=$db->id;?>"><span style="padding: 5px 7px;" class="badge badge-inverse">proses pengiriman</span></a>
                                                    <a class="set_completed dz-tip" title="update status to completed"href="<?=base_url()?>manage/order/set_completed/<?=$db->id;?>"><span style="padding: 5px 7px;" class="badge badge-inverse">pessanan diterima</span></a>
                                                <?elseif($db->order_status == 'delivery-process'):?>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">menunggu pembayaran</span></a>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">pembayaran sukses</span></a>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">proses pengiriman</span></a>
                                                    <a class="set_completed dz-tip" title="update status to completed"href="<?=base_url()?>manage/order/set_completed/<?=$db->id;?>"><span style="padding: 5px 7px;" class="badge badge-inverse">pesanan diterima</span></a>
                                                <?else: ?>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">menunggu pembayaran</span></a>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">pembayaran sukses</span></a>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">proses pengiriman</span></a>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-success">pesanan diterima</span></a>
                                                <?endif;?>

                                            <?php endif ?>


                                        </div>  
                                    </div>
                                    <?if($db->resi != ''):?>
                                    <div id="resi-delivery">
                                        <p style="margin-bottom:5px">
                                            <small><strong>No Resi : </strong></small> 
                                            <input id="text-resi" type="text" name="resi" placeholder="input no resi" class="form-control form-control-sm" value="<?=$db->resi;?>">
                                            <a id="btn-save-resi" title="save no resi" class="dz-tip tooltipstered" href="<?=base_url()?>manage/order/resi/<?=$source_id;?>"><span style="padding: 5px 7px;" class="badge">save</span></a>
                                        </p>
                                    </div>
                                    <?endif?>
                                    <div id="payment-method-inv">
                                        <p style="margin-bottom:5px;display:inline;margin-right:5px">
                                            <small><strong>Payment method : </strong></small> 
                                        </p>
                                        <div style="display:inline;width:auto">
                                            <span class="badge badge-purple"><?=$db->payment;?></span>
                                            <?=$db->payment_detail;?>
                                        </div>  
                                        
                                    </div>
                                    <?php if ($db->receipt != ""): ?>
                                    <div>
                                        <a href="<?=base_url()?>assets/receipt/<?=$db->receipt;?>"
                                                        title="show customer payment confirmation"
                                                        class="btn-confirm-download btn btn-success waves-effect waves-light btn-sm dz-tip" target="_blank"
                                                        style="margin: 10px;margin-left: 0px;font-size: 10px;"> 
                                                        <i class="fa fa-file-image-o m-r-5"></i> 
                                                        <span>payment confirmation</span> 
                                                    </a>                                                    
                                    </div>
                                    <?php endif?>
                                </div>

                            </div>

                            <div class="col-4">
                                <h5>Shipping Address</h5>

                                <address class="line-h-24">
                                    <b style="font-size: 13px;text-transform: capitalize;"><?=$db->cust_f_name;?></b><br>
                                    <?=$db->cust_address;?><br>
                                    <?=$db->city;?><br><?=$db->province;?><br>
                                    Kec.<?=$db->cust_kecamatan;?> - <?=$db->cust_zip;?><br>
                                    <abbr title="Phone">P:</abbr> <?=$db->cust_phone;?>
                                </address>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table m-t-30">
                                        <thead>
                                            <tr><th>#</th>
                                                <th>Picture</th>
                                                <th>Item</th>
                                                <th>Description</th>
                                                <th class="text-right">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>
                                                    <img width="75" src="<?=base_url()?>assets/section/<?=$db->prd_image;?>">
                                                </td>
                                                <td>
                                                    <b><?=$db->title;?></b>
                                                    <?php if ($db->order_quantity > 1): ?>
                                                        <br>QTY : <?=$db->order_quantity;?> pcs
                                                    <?php endif ?>
                                                </td>
                                                <td>
                                                    <?=$db->description;?><br>
                                                    <?=$db->description2;?>
                                                </td>
                                                <td class="text-right">Rp. <?=format_number($db->prd_price);?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6"></div>
                            <div class="col-md-5 col-6 offset-md-1">
                                <div class="float-right text-right">
                                    <?if ($db->prd_coupon > 0): ?>
                                        <p><b>Coupon (<?=$db->prd_coupon_code;?>) :</b> Rp. <?=format_number($db->prd_coupon);?> (-)</p>
                                    <?endif;?>
                                    
                                    <p><b>Shipping via <?=$db->cust_shipping;?></b> <?=format_number($db->prd_shipment)?></p>
                                    <h3>Rp. <?=format_number($db->prd_total);?></h3>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>