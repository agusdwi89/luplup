<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 class="page-title">Manage Sub-Domain</h4>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="card">
					<div class="card-body table-responsive">
						<h4 class="m-t-0 header-title">
							<b>List Sub-domain</b>
						</h4>
						<br>

						<table class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
							<thead>
								<tr>
									<th>No</th>
									<th>Subdomain</th>
									<th>URL</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $i=0;foreach ($db->result() as $d): $i++;?>
									<tr>
										<td><?=$i;?></td>
										<td><?=$d->subdomain;?></td>
										<td>
											<a href="http://<?=$d->subdomain;?>.<?=$_SERVER['HTTP_HOST']?>" target="_blank"><?=$d->subdomain;?>.<?=$_SERVER['HTTP_HOST']?></a>
										</td>
										<td>
											<center>
												<a style="opacity:100 !important;margin-right:12px;float:initial;" title="delete" href="<?=base_url()?>manage/subdomain/delete/<?=$d->id;?>" class="fa fa-times-circle delete-list dz-tip confirm-delete"></a>
											</center>
										</td>
									</tr>
								<?php endforeach ?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="card">
					<div class="card-body table-responsive">
						<h4 class="m-t-0 header-title">
							<b>Add Sub-Domain</b>
						</h4>
						<br>				
						<?=form_open(base_url('manage/subdomain/add'),array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="col-md-12 control-label">Subdomain name</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="title text" class="form-control" name="subdomain" value="">
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <hr>
                            <small>to maintain the consistency of old data, the edit feature is disable, if you want change the new subdomain that is incorrect, please delete and create a new one</small>
                        <?=form_close()?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>