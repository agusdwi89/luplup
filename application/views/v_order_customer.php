<!DOCTYPE html>
<html lang="en">
	<head>
		<?=$this->load->view('fe/header');?>	
		<script type="text/javascript">
			var clog;
		</script>
	</head>

<body>
	<?=form_open('/',array('id'=>'global-form'))?>
	<?=form_close()?>

	<!-- Start Preloader -->
	<div id="page-preloader">
		<svg class="circular" height="50" width="50">
			<circle class="path" cx="25" cy="25" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"></circle>
		</svg>
	</div>
	<!-- End Preloader -->

	<!-- Start Content -->
	<section id="main" class="wrapper">
		<?=$this->load->view('fe/nav2');?>

		<section id="products-2" class="products block gray">
			<div class="container">
				<div class="row">
					<br>
					<h3 style="text-align: center">Order Detail</h3>
					<div class="block-desc"><p>order id : #SB1<?=sprintf("%'.05d\n", $db->id);?></p></div>
					<br>
					<?
						$d['db'] 		= $db;
						$this->load->view('fe/invoice_front',$d);
					?>
				</div>
			</div>

		</section>
	</section>
	<!-- End Content -->
	<?=$this->load->view('fe/footer_script');?>
	<?=$this->load->view('fe/i_popup');?>

	<?=$this->load->view('fe/ga');?>

</html>