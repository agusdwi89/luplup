<!DOCTYPE html>
<html>
<head>
	<title>Print</title>
	<link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body style="width:800px;font-size:70%;margin:10px auto">

<div class="content">
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<div class="clearfix">
							<div class="float-left">
							</div>
							<h5 class="m-0">
								Order :: #SB1<?=sprintf('%05d', $db->id);?> - <?=$db->cust_f_name;?>
								<a style="float: right;" id="btn-save-order" target="_blank" href="javascript:window.print()" class="badge badge-info">print</a>
							</h5>
						</div>

						<div class="row m-t-30">
							<div class="col-7">
								<br>
								<h5>Order Detail</h5>

								<div >
									<p style="margin-bottom:5px"><small><strong>Order Date: </strong></small> <?=pretty_date($db->time_order);?></p>
									<p style="margin-bottom:5px"><small><strong>Order ID: </strong></small> #SB1<?=sprintf('%05d', $db->id);?></p>
									<div style="margin-bottom:5px">
										<small><strong>Order Status: </strong></small> 
										<div class="btn-group m-l-10">
											<?if ($db->order_status == "wait-payment"): ?>
												<span class="badge badge-danger">Menunggu Pembayaran</span>
											<?elseif($db->order_status == "payment-success"):?>
												<span class="badge badge-info">Pembayaran Sukses</span>
											<?elseif($db->order_status == "delivery-process"):?>
												<span class="badge badge-info">Proses Pengiriman</span>
											<?else: ?>
												<span class="badge badge-success">Pesanan diterima</span>
											<?endif;?>
										</div>  
									</div>
									<?if($db->resi != ''):?>
									<div id="resi-delivery">
										<p style="margin-bottom:5px">
											<small><strong>No Resi : </strong></small> 
											<?=$db->resi;?>
										</p>
									</div>
									<?endif?>
									<div id="payment-method-inv">
										<p style="margin-bottom:5px;display:inline;margin-right:5px">
											<small><strong>Payment method : </strong></small> 
										</p>
										<div style="display:inline;width:auto">
											<span class="badge badge-purple"><?=$db->payment;?></span>
											<?=$db->payment_detail;?>
										</div>  
									</div>
								</div>

							</div>

							<div class="col-5">
								<br><br>
								<h5>Shipping Address</h5>

								<address class="line-h-24">
									<b style="font-size: 13px;text-transform: capitalize;"><?=$db->cust_f_name;?></b><br>
									<?=$db->cust_address;?><br>
									<?=$db->city;?><br><?=$db->province;?><br>
                                    Kec.<?=$db->cust_kecamatan;?> - <?=$db->cust_zip;?><br>
                                    <abbr title="Phone">P:</abbr> <?=$db->cust_phone;?>
								</address>

							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table m-t-30">
										<thead>
											<tr><th>#</th>
												<th>Picture</th>
												<th>Item</th>
												<th>Description</th>
												<th class="text-right">Price</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>
													<img width="75" src="<?=base_url()?>assets/section/<?=$db->prd_image;?>">
												</td>
												<td>
													<b><?=$db->title;?></b>
													<?php if ($db->order_quantity > 1): ?>
														<br>QTY : <?=$db->order_quantity;?> pcs
													<?php endif ?>
												</td>
												<td>
													<?=$db->description;?><br>
													<?=$db->description2;?>
												</td>
												<td class="text-right">Rp. <?=format_number($db->prd_price);?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6"></div>
							<div class="col-md-5 col-6 offset-md-1">
								<div class="float-right text-right">
									<?if ($db->prd_coupon > 0): ?>
									<p><b>Coupon (<?=$db->prd_coupon_code;?>) :</b> Rp. <?=format_number($db->prd_coupon);?> (-)</p>
									<?endif;?>

									<p><b>Shipping via <?=$db->cust_shipping;?></b> <?=format_number($db->prd_shipment)?></p>
									<h3>Rp. <?=format_number($db->prd_total);?></h3>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>

</html>
