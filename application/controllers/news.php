<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->db->get_where('_subdomain',array('subdomain'=>subdomain()))->num_rows() == 0) {
			show_404();
		 }
	}

	function index(){
		$data['list_section']	= $this->get_footer();
		$data['news'] 			= $this->db->order_by('date','desc')->get('news');
		$this->load->view('v_news',$data);
	}

	function read($slug){
		$item = $this->db->limit(1)->get_where('news',array('slug'=>$slug));
		if($item->num_rows() == 0) show_404();
		
		$data['list_section']	= $this->get_footer();
		$data['item'] 			= $item->row();
		$this->load->view('v_news_detail',$data);	
	}

	private function get_footer(){
		$all_list = [];
		$list = $this->db->get_where('view_list_section',array('subdomain'=>subdomain(),'db_table'=>'section_footer'));

		foreach ($list->result() as $key) {
			if ($key->db_table == "news") 
				$ls = $this->db->limit(1)->get($key->db_table);
			else $ls = $this->db->get_where($key->db_table,array('id' => $key->section_id));
			
			if ($ls->num_rows == 1) {
				$all_list[] = array(
					'layout'	=> $key->db_table,
					'data'		=> $ls->result()[0]
				);
			}
		}

		return $all_list;
	}
}