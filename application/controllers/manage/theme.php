<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Theme extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "theme";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}

	public function index()
	{
		if (is_post()) {
			$data = array('value' => $this->input->post('theme'));
			$this->db->where('subdomain', $this->sub_domain);
			$this->db->where('name', 'theme');
			$this->db->update('site_config', $data); 

			if ($this->input->post('theme') == "custom") {
				$this->generate_custom_theme();
			}

			$this->session->set_flashdata('message','theme config saved successfully');
			redirect(base_url('manage/theme'));
		}

		$theme_config 			= $this->db->get_where('site_config',array('subdomain'=>$this->sub_domain,'name'=>'theme'))->row();
		$data['local_view'] 	= 'v_theme';
		$data['theme_choosen'] 	= $theme_config->value;
		$data['custom_color'] 	= json_decode($theme_config->value2);
		$this->load->view('v_manage',$data);
	}

	function generate_custom_theme(){
		$cst 	= $this->input->post('custom');
		$json 	= json_encode($cst);

		// readfile & write file
		$this->load->helper('file');
		$string = read_file('assets/fe/css/colors/def.css');
		foreach ($cst as $key => $value) {
			$string = str_replace("{".$key."}", $value, $string);
		}
		write_file('assets/fe/css/colors/custom.css', $string);

		//update db
		$data = array('value2' => $json);
		$this->db->where('subdomain', $this->sub_domain);
		$this->db->where('name', 'theme');
		$this->db->update('site_config', $data);
	}

	function custom(){
		if (is_post()) {
			
		}
	}

	function readdefcss(){
		$this->load->helper('file');
		$string = read_file('assets/fe/css/colors/def.css');
		$string = str_replace("defcolor1", "agusdwip", $string);
		debug_array($string);
	}
}