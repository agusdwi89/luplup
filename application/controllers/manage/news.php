<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "news";
		$this->sub_domain = $this->session->userdata('session_subdomain');
		$this->slug = "";
	}

	public function index(){
		$data['db'] 		= $this->db->order_by("date", "desc")->get('news');
		$data['local_view'] = 'news/v_index';
		$this->load->view('v_manage',$data);
	}

	public function add(){
		if (is_post()) {
			$data = $this->input->post();
			$this->get_slug($data['title']);
			$data['img_thumbnail']	= $this->upload('img_thumbnail','single');
			$data['img_header']		= $this->upload('img_header','single');
			$data['slug']			= $this->slug;
			$data['content']		= $this->remove_license($data['content']);
			$this->db->insert('news',$data);
			$this->session->set_flashdata('message','Data saved successfully');
			redirect(base_url('manage/news'));
		}
		$data['local_view'] = 'news/v_add';
		$this->load->view('v_manage',$data);
	}

	function edit($id){
		$item = $this->db->get_where('news',array('id'=>$id))->row();
		if (is_post()) {
			$data = $this->input->post();
			$this->get_slug($data['title']);
			$img_thumbnail 	= $this->upload('img_thumbnail','single');
			$img_header 	= $this->upload('img_header','single');

			$data['slug'] 			= ($item->title == $data['title'])? $item->slug : $this->slug;	
			$data['img_thumbnail']	= ($img_thumbnail == "")? $item->img_thumbnail : $img_thumbnail;
			$data['img_header']		= ($img_header == "")? $item->img_header : $img_header;

			$this->db->where('id', $id);
			$this->db->update('news', $data); 

			$this->session->set_flashdata('message','Data saved successfully');
			redirect(base_url('manage/news'));
		}
		$data['item'] = $item;
		$data['local_view'] = 'news/v_edit';
		$this->load->view('v_manage',$data);	
	}

	function delete($id){
		$this->db->delete('news', array('id' => $id)); 
		$this->session->set_flashdata('message','Data deleted successfully');
		redirect(base_url('manage/news'));
	}

	function upload($input="userfile",$state="inline"){
		$_FILES[$input]['name']	= strtolower($_FILES[$input]['name']);
		$config['upload_path']		= 'assets/news';
		$config['allowed_types']	= 'jpg|png';
		$config['max_size']			= '10000';
		$config['max_width']		= '5000';
		$config['max_height']		= '5000';
		$config['encrypt_name']		= true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload($input)){
			if ($state=="single") return "";
			else return array(false,$this->upload->display_errors());
		}else{
			$a 		= $this->upload->data();
			$url 	= base_url().'assets/news/'.$a['file_name'];
			if ($state=="single") return $a['file_name'];
			else{
				$b['link'] = $url;
				echo json_encode($b);
			}
		}
	}

	private function get_slug($title,$num=0){
		$t 		= ($num > 0)? $title.$num : $title ;
		$slug 	= url_title($t,'-',true);
		$cek 	= $this->db->limit(1)->get_where('news',array('slug'=>$slug))->num_rows();
		if ($cek >0) {
			$n = $num+1;
			$this->get_slug($title,$n);
		}else $this->slug = $slug;
	}

	private function remove_license($text){
		$s = '<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>';
		return str_replace($s, "", $text);

	}
}