<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->db->get_where('_subdomain',array('subdomain'=>subdomain()))->num_rows() == 0) {
			show_404();
		 }
	}

	public function index()
	{
		$this->counting();
		$all_list = [];
		$list = $this->db->get_where('view_list_section',array('subdomain'=>subdomain()));

		foreach ($list->result() as $key) {
			if ($key->db_table == "news") 
				$ls = $this->db->limit(1)->get($key->db_table);
			else $ls = $this->db->get_where($key->db_table,array('id' => $key->section_id));
			
			if ($ls->num_rows == 1) {
				$all_list[] = array(
					'layout'	=> $key->db_table,
					'data'		=> $ls->result()[0]
				);
			}
		}

		$data['list_section']	=	$all_list;
		$this->load->view('v_home',$data);
	}

	function delivery(){
		$data['form'] = $this->input->post();
		$this->load->view('fe/i_popup_payment',$data);
	}

	function confirmation(){
		$data = array();
		$this->load->view('fe/i_confirm',$data);
	}

	function check_coupon(){
		if (is_post()) {
			$code = $this->input->post('code');
			$coupon = $this->db->get_where('coupon',array('code'=>$code,'active'=>'yes', 'valid >= '=> date("Y-m-d")));
			if ($coupon->num_rows == 1) {
				echo json_encode(array('status'=>'valid','value'=>$coupon->row()->discount));
			}else{
				echo json_encode(array('status'=>'invalid','value'=>0));
			}
		}
	}

	function place_order(){
		if (is_post()) {
			$data = $this->input->post();

			$prd = $this->db->get_where('section_product_items',array('id'=>$data['prd_id']))->row();

			$data['prd_title']			= $prd->title;
			$data['prd_description']	= $prd->description;
			$data['prd_description2']	= $prd->description2;
			$data['prd_image']			= $prd->image;

			if ($prd->price_dsc > 0) {
				$data['prd_price'] = $prd->price_dsc;
				$data['prd_price_tag'] = $prd->price;
			} else {
				$data['prd_price'] = $prd->price;
				$data['prd_price_tag'] = $prd->price;
			}

			$shipment 				= $this->db->get_where('mst_delivery',array("id_kecamatan"=>$data['cust_kecamatan_id']))->row_array();

			$data['prd_shipment'] 	= $shipment[$data['cust_shipping']];
			
			$kupon 					= $this->cekKupon($data['prd_coupon_code']);
			if ($kupon['status'] == 'valid') {
				$data['prd_coupon'] = $kupon['value'];
			} else {
				$data['prd_coupon'] = 0;
				$data['prd_coupon_code'] = "";
			}
			
			$gtotal = ($data['prd_price']*$data['order_quantity']) + $data['prd_shipment'] - $data['prd_coupon'];

			if ($gtotal == $data['prd_total']) {
				$data['link_unique'] 	= $this->generate_link_number($data['cust_email']);
				$data['subdomain'] 		= subdomain();
				$this->db->insert('orders',$data);

				$this->email($this->db->insert_id(),$data['cust_email']);
				
				echo json_encode(array('status'=>'success','message'=>'ready to save data','link_unique'=>$data['link_unique'],'payment'=>$data['payment']));
			} else {
				echo json_encode(array('status'=>'failed','message'=>'total not same'));
			}
		}
	}

	private function cekKupon($code){
		$coupon = $this->db->get_where('coupon',array('code'=>$code,'active'=>'yes', 'valid >= '=> date("Y-m-d")));
		if ($coupon->num_rows == 1) {
			return array('status'=>'valid','value'=>$coupon->row()->discount);
		}else{
			return array('status'=>'invalid','value'=>0);
		}
	}

	function generate_link_number($email){
		$char = preg_replace('/[^A-Za-z0-9\-]/', '', $email);

		$t = microtime(true);
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		$d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
		return $char."-".$d->format("u-mdHYis"); // note at point on "u"
	}

	function detail($uniqid){
		$data = $this->db->get_where('orders',array('link_unique'=>$uniqid));
		debug_array($data);
	}

	function order($link_unique){
		$data['db'] 		= $this->db->get_where('v_manage_order',array('link_unique'=>$link_unique))->row();
		$this->load->view('v_order_customer',$data);
	}

	function email($id=2,$email="agusdwi89@gmail.com"){
		$data['db'] = $this->db->get_where('v_manage_order',array('id'=>$id))->row();
		
		$d = $this->load->view('v_email',$data,true);

		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'shopbay.id',
			'smtp_pass' => 'Zoomy123456!',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1'
			);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('shopbay.id@gmail.com', 'Shop Bay Indonesia');
		$this->email->to($email); 

		$this->email->subject('Tagihan dan Petunjuk Pembayaran : '.$data['db']->prd_title);
		$this->email->message($d);  

		$this->email->send();
	}

	function success($unq){
		$data['db']			= $this->db->get_where('orders',array('link_unique'=>$unq));
		$this->load->view('v_order_finish',$data);
	}

	function payment_confirm($id,$callback){
		if (is_post()) {
			$_FILES['userfile']['name']		= strtolower($_FILES['userfile']['name']);
			$config['upload_path']			= 'assets/receipt';
			$config['allowed_types']		= 'jpg|png';
			$config['max_size']				= '10000';
			$config['max_width']			= '800000';
			$config['max_height']			= '1024000';
			$config['encrypt_name']			= true;
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload()){
				echo "<body style='font-family: Montserrat,Arial,sans-serif !important;'><br><br><center><h5>Gambar gagal di upload <br><br> pastikan bertipe JPG / PNG & ukuran kurang dari 2 Mb</h5><center><br>";
				echo "<center><a href='".base_url('home/order/'.$callback)."'>back</a></center></body>";
				die();
			}else{
				$a = $this->upload->data();

				$data = array('receipt' =>  $a['file_name']);
				$this->db->where('id', $id);
				$this->db->update('orders', $data);
				
				redirect(base_url('home/order/'.$callback));
			}
		} 
	}

	function get_kec(){
		$i = $this->input->post('city_id');
		$data = $this->db->get_where('mst_delivery',array('id_city'=>$i));
		$rr = array();
		foreach ($data->result() as $k => $d) {
			$rr[$k] = $d;
			$rr[$k]->kurir = $this->generate_kurir($d);
		}
		echo json_encode($data->result());
	}

	function generate_kurir($k){
		$rt = array();
		if ($k->jne >0) 
			$rt[] = array('name' => 'jne','price' => $k->jne);
		if ($k->jnt >0) 
			$rt[] = array('name' => 'jnt','price' => $k->jnt);
		if ($k->ninja >0) 
			$rt[] = array('name' => 'ninja','price' => $k->ninja);
		if ($k->wahana >0) 
			$rt[] = array('name' => 'wahana','price' => $k->wahana);
		if ($k->pos >0) 
			$rt[] = array('name' => 'pos','price' => $k->pos);

		return $rt;
	}

	function counting(){
		$data['ip'] 		= $this->input->ip_address();
		$data['subdomain'] 	= subdomain();
		$this->db->insert('visitor',$data);
	}

	function tesemailjbd(){
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'jakartabubble@gmail.com',
			'smtp_pass' => 'Jakarta1234',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1'
			);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('jakartabubble@gmail.com', 'Tes Email aja');
		$this->email->to("agusdwi89@gmail.com"); 

		$this->email->subject('ini coba');
		$this->email->message("halo halo halo");  

		$this->email->send();

		echo $this->email->print_debugger();
	}
}