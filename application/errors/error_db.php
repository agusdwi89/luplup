<?php
    $home404    = "https://www.luplupbubbledrink.com/";
    $url404     = $home404."/404/";
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8"/>
    <title>luplupbubbledrink.com - PAGE NOT FOUND 404</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="luplupbubbledrink.com"/>
    <meta name="keywords" content="luplupbubbledrink.com"/>
    <meta name="description" content="luplupbubbledrink.com - Error page"/>
    
    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo $url404;?>css/style.css">

    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" media="all" href="css/ie.css" />
    <script type="text/javascript" src="js/html5.js" ></script>
    <![endif]-->

    <!-- Javascripts -->
    <script type="text/javascript" charset="utf-8" src="<?php echo $url404;?>js/jquery-1.8.3.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo $url404;?>js/plax.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo $url404;?>js/404.js"></script>
</head>
    <body id="errorpage" class="error404">
        <div id="pagewrap">
            <!-- Plane -->
            <div id="main-content">
                <div class="duck-animation" style="background-image:url('<?php echo $url404;?>images/plane.png');"></div>
            </div>
            <div id="wrapper" class="clearfix">     
                <div id="parallax_wrapper">    
                    <div id="content">
                        <h1>Lost in the Clouds<br />Flight not Found</h1>
                        <p>The page you're looking for is not here.</p>
                        <a href="<?php echo $home404;?>" title="" class="button">Go Home</a>
                    </div>
                    <span class="scene scene_1"></span>
                    <span class="scene scene_2"></span>
                    <span class="scene scene_3"></span>
                </div>
            </div>
        </div>
        <div id="footer">  
            <div class="container">
                <ul class="copyright_info">
                    <li>&copy; 2019 luplupbubbledrink.com</li>
                    <li>&middot;</li>
                    <li></li>
                </ul>
            </div>
        </div>

        <!-- 
            <h1><?php echo $heading; ?></h1>
            <?php echo $message; ?>
         -->
        
    </body>
</html>